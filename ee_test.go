package eerrors

import (
	"bytes"
	"errors"
	"fmt"
	"runtime"
	"strconv"
	"strings"
	"testing"
	"time"
)

func begin() {
	New(405, 405, "", "", 3)
	New(111111, 501, "Текст для лога  #1", "Текст для пользователя #1", ERROR)
	New(222222, 502, "Текст для лога  #2", "Текст для пользователя #2", ERROR)
}

func TestNew(t *testing.T) {
	begin()
	
	_, ok := New(111111, 501, "Текст для лога  #1", "Текст для пользователя #1", 3)
	if ok {
		t.Error("Ошибка не могла создаться, но создалась")
	}
	_, ok = New(333333, 501, "Текст для лога  #1", "Текст для пользователя #1", 3)
	if !ok {
		t.Error("Ошибка д.б. создаться, но не создалась")
	}
}

func TestError_IsEq(t *testing.T) {
	fName := "ee_test.go"
	fnName := "TestOne"
	lp := LogPrefixType("6qyftytfd76")
	
	begin()
	ee, _ := New(333, 503, "Текст для лога  #3", "Текст для пользователя #3", ERROR)
	e := Decorate(333, fName, fnName).WithError(errors.New("new error #1")).WithPrefix(lp).WithStringNum(57)
	if !ee.IsEq(*e) {
		t.Error("неверно #1")
	}
	
	e = Decorate(111111, fName, fnName).WithError(errors.New("new error #1")).WithPrefix(lp).WithStringNum(57)
	if e.IsEq(*ee) {
		t.Error("неверно #2")
	}
}

func TestError_Is(t *testing.T) {
	fName := "ee_test.go"
	fnName := "TestError_Is"
	lp := LogPrefixType("ejrlhejk111")
	
	begin()
	ee, _ := New(333, 503, "Текст для лога  #3", "Текст для пользователя #3", ERROR)
	e := Decorate(333, fName, fnName).WithError(errors.New("new error #1")).WithPrefix(lp).WithStringNum(57)
	if !ee.IsEq(*e) {
		t.Error("неверно #1 Message")
	}
	if !ee.Is(e) {
		t.Error("неверно #1 *Message")
	}
	if !ee.Is(333) {
		t.Error("неверно #1 int")
	}
	if !ee.Is(int64(333)) {
		t.Error("неверно #1 int64")
	}
	
	e = Decorate(111111, fName, fnName).WithError(errors.New("new error #2")).WithPrefix(lp).WithStringNum(57)
	t.Logf("ee: %#v; e: %#v", ee, e)
	if e.Is(*ee) {
		t.Error("неверно #2 Message")
	}
	if e.Is(ee) {
		t.Error("неверно #2 *Message")
	}
	if !e.Is(111111) {
		t.Error("неверно #2 int")
	}
	if !e.Is(int64(111111)) {
		t.Error("неверно #2 int64")
	}
}

func TestOne(t *testing.T) {
	begin()
	
	fName := "ee_test.go"
	fnName := "TestOne"
	lp := LogPrefixType("6qyftytfd76")
	
	println("НАЧАЛЬНОЕ СОЗДАНИЕ ОШИБКИ: " + `New(111111, 501, "Текст для лога  #1", "Текст для пользователя #1", ERROR)` + "\n")
	// ПРИМЕР
	e := Decorate(111111, fName, fnName)
	// вывод ошибки в лог
	println("ВЫЗОВ : ", `e := Decorate(111111, "ee_test.go", "TestOne")`)
	println("В ЛОГЕ: ", e.Error(), "\n")
	if !strings.Contains(e.Error(), `111111 Текст для лога  #1 @[ee_test.go:TestOne]`) {
		t.Error("Ошибка #1")
	}
	
	e = Decorate(111111, fName, fnName).WithError(errors.New("new error #1"))
	println("ВЫЗОВ : ", `e := Decorate(111111, "ee_test.go", "TestOne").WithError(errors.New("new error #1"))`)
	println("В ЛОГЕ: ", e.Error(), "\n")
	if !strings.Contains(e.Error(), `111111 Текст для лога  #1 new error #1 @[ee_test.go:TestOne]`) {
		t.Error("Ошибка #2", e.Error())
	}
	
	e = Decorate(111111, fName, fnName).WithError(errors.New("new error #1")).WithPrefix(lp)
	println("ВЫЗОВ : ", `e := Decorate(111111, "ee_test.go", "TestOne").WithError(errors.New("new error #1")).WithPrefix(lp)`)
	println("В ЛОГЕ: ", e.Error(), "\n")
	if !strings.Contains(e.Error(), `6qyftytfd76 ERROR 111111 Текст для лога  #1 new error #1 @[ee_test.go:TestOne]`) {
		t.Error("Ошибка #3", e.Error())
	}
	
	e = Decorate(111111, fName, fnName).WithError(errors.New("new error #1")).WithPrefix(lp).WithStringNum(57)
	println("ВЫЗОВ : ", `e := Decorate(111111, "ee_test.go", "TestOne").WithError(errors.New("new error #1")).WithPrefix(lp).WithStringNum(57)`)
	println("В ЛОГЕ: ", e.Error(), "\n")
	if !strings.Contains(e.Error(), `6qyftytfd76 ERROR 111111 Текст для лога  #1 new error #1 @[ee_test.go:TestOne:57]`) {
		t.Error("Ошибка #4", e.Error())
	}
	
	// вывод ошибки для пользователя
	println("REST:   ", string(e.JSON()))
	if string(e.JSON()) != `{"errorCode":501,"errorMessage":"Текст для пользователя #1"}` {
		t.Error("Ошибка #5", string(e.JSON()), " != ", "{\"errorCode\":501,\"errorMessage\":\"Текст для пользователя #1\"}")
	}
}

func TestGetNow(t *testing.T) {
	for i := 0; i < 10000; i++ {
		tt := time.Now()
		s := getTimeString(tt)
		// формат: 2022-02-10T12:45:10.234548+03:00
		if len(s) != 32 {
			t.Errorf("wrong: %s (%d)\n", s, len(s))
		}
	}
}

func TestMustGet(t *testing.T) {
	lp := LogPrefixType("jy7fdnifheq145")
	if _, ok := New(444, 0, "Текст для дебага", "", DEBUG); !ok {
		t.Error("TestGet не получилось создать новую ошибку")
	}
	
	d, ok := MustGet(444)
	if !ok {
		t.Error("TestGet не нашли созданную ошибку")
	}
	d.WithPrefix(lp).WithMessage("что-то выводим", 22, 22.222).WOCode().WithAt("ee_test", "TestGet")
	fmt.Printf("Debug: %s\n", d)
	
	d, ok = MustGet(555)
	if ok || d != nil {
		t.Error("Нашли ошибку которой быть не должно")
	}
}

func TestGet(t *testing.T) {
	lp := LogPrefixType("jy7fdnifheq145")
	if _, ok := New(4444, 0, "Текст для дебага", "", DEBUG); !ok {
		t.Error("TestGet не получилось создать новую ошибку")
	}
	
	d := Get(4444).WithPrefix(lp).WithMessage("что-то выводим", 22, 22.222).WOCode().WithAt("ee_test", "TestGet")
	fmt.Printf("Debug: %s\n", d)
}

func TestDebug(t *testing.T) {
	lp := LogPrefixType("fhdjkfs6t6fe")
	d := Get(0).WithMessage("что-то выводим", 22, 22.222)
	fmt.Printf("%s\n", d)
	d = Get(0).WithMessage("что-то выводим", 22, 22.222).WithPrefix(lp)
	fmt.Printf("%s\n", d)
	d = Get(0).WithMessage("что-то выводим", 22, 22.222).WithPrefix(lp).WithAt("ee_test", "TestDebug")
	fmt.Printf("%s\n", d)
}

func TestMessage_WOTime(t *testing.T) {
	begin()
	lp := LogPrefixType("fhdjkfs6t6fe")
	e := Get(111111)
	fmt.Printf("wotime=%s\n", e)
	
	e = e.WOTime()
	fmt.Printf("wotime=%s\n", e)
	
	e = e.WithPrefix(lp)
	fmt.Printf("wotime=%s\n", e)
	
	e = e.WOCode()
	fmt.Printf("wotime=%s\n", e)
	
	if strings.Contains(e.String(), "[") {
		t.Error("время не удалилось")
	}
}

func TestSetModuleName(t *testing.T) {
	begin()
	mName := "testModule"
	
	e := Get(111111)
	fmt.Printf("before=%s\n", e)
	if strings.Contains(e.String(), mName) {
		t.Error("ошибка #1")
	}
	
	SetModuleName(mName)
	
	e = Get(111111)
	fmt.Printf("after=%s\n", e)
	if !strings.Contains(e.String(), mName) {
		t.Error("ошибка #2")
	}
	
	e = Get(111111).WOModuleName()
	fmt.Printf("after=%s\n", e)
	if strings.Contains(e.String(), mName) {
		t.Error("ошибка #3")
	}
}

func TestMessage_Sprintf(t *testing.T) {
	New(1, 0, "Строка=%s, число=%d", "", 3)
	e := Get(1).Sprintf("aaa", 22)
	if !strings.Contains(e.String(), "Строка=aaa, число=22") {
		t.Errorf("не работает Sprintf:\n\tждали: Строка=aaa, число=22\n\tполучили=%s", e.String())
	}
}

func TestMessage_SprintfPriv(t *testing.T) {
	New(1777, 0, "Строка=%s, число=%d", "публичная ошибка", 3)
	e := Get(1777).SprintfPriv("aaa", 22)
	if !strings.Contains(e.String(), "Строка=aaa, число=22") {
		t.Errorf("не работает SprintfPriv:\n\tждали: Строка=aaa, число=22\n\tполучили=%s", e.String())
	}
	
	if e.publicMessage != "публичная ошибка" {
		t.Errorf("не работает SprintfPriv:\n\tждали строку \"публичная ошибка\" \n\tа получили :: %s", e.publicMessage)
	}
}

func TestBG(t *testing.T) {
	for i := 0; i <= 100; i++ {
		e := Debug().WithMessage("i=", strconv.Itoa(i+1))
		println(e.Error())
	}
}

func TestMessage_PublicError(t *testing.T) {
	begin()
	
	e := Get(111111)
	ec, em := e.PublicError()
	fmt.Printf("ec=%d em=\"%s\"\n", ec, em)
	if !strings.Contains(em, "111111") {
		t.Error("Не добавился код")
	}
	
	e = Error()
	ec, em = e.PublicError()
	fmt.Printf("ec=%d em=\"%s\"\n", ec, em)
	if len(em) > 0 {
		t.Error("Ошибка #2")
	}
}

func TestMessage_PrivateError(t *testing.T) {
	begin()
	
	// Тест если не заполнено поле PrivateError
	e := Get(405)
	ec, em := e.PrivateError()
	fmt.Printf("ec=%d em=\"%s\"\n", ec, em)
	
	e = Get(405).WithMessage(`Message for error
{
    "one":1,
    "two": 2
}
    `)
	ec, em = e.PrivateError()
	fmt.Printf("ec=%d em=\"%s\"\n", ec, em)
	
	e = Error().WithMessage("Message #2 for error").WithError(errors.New("this is error text"))
	ec, em = e.PrivateError()
	fmt.Printf("==>1::ec=%d em=\"%s\"\n", ec, em)
	ec, em = e.PrivateErrorWithErr()
	fmt.Printf("==>2::ec=%d em=\"%s\"\n", ec, em)
	
	e, _ = New(100, 2000, "private", "public", NOTICE)
	ec, em = e.PublicError()
	_, em = e.PrivateErrorWithErr()
	fmt.Printf("3::ec=%d em=\"%s\"\n", ec, em)
}

// func BenchmarkPrepare(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		eerr := Error().WithPrefix(LogPrefixType("LP")).WithAt("ee_test.go", "BenchmarkPrepare").WithMessage("Это проверка #", 1)
// 		eerr.prepareOld()
// 	}
// }
//
// func BenchmarkPrepareBuilder(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		eerr := Error().WithPrefix(LogPrefixType("LP")).WithAt("ee_test.go", "BenchmarkPrepare").WithMessage("Это проверка #", 1)
// 		eerr.prepare()
// 	}
// }

func TestLogLevel(t *testing.T) {
	logPrefix := NewPrefix()
	d := Debug().WithPrefix(logPrefix).WithMessage("Debug").prepare()
	e := Error().WithPrefix(logPrefix).WithMessage("Error").prepare()
	if len(d) == 0 || len(e) == 0 {
		t.Errorf("Ошибка #1:\n\td=%s\n\te=%s\n", d, e)
	}
	SetCurLogLevel(3)
	d = Debug().WithPrefix(logPrefix).WithMessage("Debug").prepare()
	e = Error().WithPrefix(logPrefix).WithMessage("Error").prepare()
	if len(d) != 0 || len(e) == 0 {
		t.Errorf("Ошибка #2:\n\td=%s\n\te=%s\n", d, e)
	}
	SetCurLogLevel(1)
	d = Debug().WithPrefix(logPrefix).WithMessage("Debug").prepare()
	e = Error().WithPrefix(logPrefix).WithMessage("Error").prepare()
	if len(d) != 0 || len(e) != 0 {
		t.Errorf("Ошибка #2:\n\td=%s\n\te=%s\n", d, e)
	}
}

func TestSetLevel(t *testing.T) {
	logPrefix := NewPrefix()
	d := Debug().WithPrefix(logPrefix).WithMessage("Debug")
	if d.level != 7 {
		t.Error("ошибка инициализации")
	}
	d = d.SetLevel(ERROR)
	if d.level != 3 {
		t.Error("ошибка смены уровня #1")
	}
	
	d = d.SetLevel(WARNING)
	if d.level != 4 {
		t.Error("ошибка смены уровня #2")
	}
}

func TestMaskedWords(t *testing.T) {
	var (
		maskPrivate = "mask_private"
		maskPublic  = "mask_public"
		maskMessage = "mask_message"
		maskError   = "mask_error"
	)
	
	SetCurLogLevel(ERROR)
	maskedWords := []string{maskPrivate, maskMessage, maskError, maskPublic}
	
	var (
		e    *Message
		ok   bool
		code = 77077
	)
	
	if e, ok = New(code, 7, maskPrivate, maskPublic, ERROR); !ok {
		t.Error("не создалась переменная типа Message")
	}
	
	e = e.MaskWords(maskedWords).WithError(errors.New(maskError)).WithMessage(maskMessage)
	
	if e.message == nil {
		t.Error("ошибка при вызове WithMessage")
	}
	
	// проверяем добавление
	if len(maskedWords) != len(e.maskedWords) {
		t.Error("ошибка добавления слов для маскирования")
	}
	
	// проверяем маскирование
	e.prepare()
	
	if e.result == nil {
		t.Error("ошибка при prepare, result = nil")
	}
	
	if strings.Contains(*e.result, maskMessage) || strings.Contains(*e.result, maskPrivate) || strings.Contains(*e.result, maskError) {
		t.Errorf("ошибка маскирования слов, e.result :: \n%s\n", *e.result)
	}
	
	// проверяем функцию PublicError()
	_, mes := e.PublicError()
	
	if strings.Contains(mes, maskPublic) {
		t.Errorf("ошибка маскирования слов, PublicMessage :: \n%s\n", mes)
	}
}

func TestTime(t *testing.T) {
	var a []*Message
	for i := 1; i <= 10; i++ {
		a = append(a, Debug().WithMessage("idx="+strconv.Itoa(i)))
		time.Sleep(time.Second / 2)
	}
	fmt.Printf("a=%#v\n", a)
}

func TestNewStringPrefix(t *testing.T) {
	str := "newprefix"
	lp := NewStringPrefix(str)
	if str != lp.String() {
		t.Errorf("%+v != %+v", str, lp.String())
	}
}

func TestAddStringPrefix(t *testing.T) {
	str := "customstr"
	lp := NewStringPrefix("newprefix")
	logPrefix := AddStringPrefix(lp, str)
	if "newprefix"+"::"+str != logPrefix.String() {
		t.Errorf("%+v != %+v", "newprefix"+"::"+str, logPrefix.String())
	}
}

func TestGetPrivf(t *testing.T) {
	_, _ = New(999991, 999991, "format me with %s", "but no me", 3)
	eerr := GetPrivf(999991, "text")
	if eerr.privateMessage != "format me with text" {
		t.Errorf("%+v != %+v", "format me with text", eerr.privateMessage)
	}
	if eerr.publicMessage != "but no me" {
		t.Errorf("%+v != %+v", "but no me", eerr.publicMessage)
	}
}

func TestGetf(t *testing.T) {
	_, _ = New(999999, 999999, "format me with %s", "and me to: %s", 3)
	eerr := Getf(999999, "text")
	if eerr.privateMessage != "format me with text" {
		t.Errorf("%+v != %+v", eerr.privateMessage, "format me with text")
	}
	if eerr.publicMessage != "and me to: text" {
		t.Errorf("%+v != %+v", eerr.publicMessage, "and me to: text")
	}
}

func TestPublicError3(t *testing.T) {
	errText := "это тестовая ошибка"
	err := errors.New(errText)
	e, _ := New(501000, 501, "приват", "паблик", 3)
	
	ec, em := PublicError(err)
	if ec != 500 || em != errText {
		t.Errorf("Ошибка #1: ec=%d, em=!%s!\n", ec, em)
	}
	
	ec, em = PrivateError(err)
	if ec != 500 || em != errText {
		t.Errorf("Ошибка #2: ec=%d, em=!%s!\n", ec, em)
	}
	
	ec, em = e.PublicError()
	if ec != 501 || !strings.HasPrefix(em, "паблик") {
		t.Errorf("Ошибка #3: ec=%d, em=!%s!\n", ec, em)
	}
	
	ec, em = e.PrivateError()
	if ec != 501000 || em != "приват" {
		t.Errorf("Ошибка #4: ec=%d, em=!%s!\n", ec, em)
	}
}

func TestName(t *testing.T) {
	lp := NewPrefix()
	var stat runtime.MemStats
	for j := 0; j < 10; j++ {
		for i := 0; i < 1_000; i++ {
			e := Get(-7)
			e = e.WithPrefix(lp)
			e.maskedWords = nil
			e = nil
		}
		runtime.ReadMemStats(&stat)
		fmt.Printf("%d::%d\n", j, stat.HeapInuse)
	}
	runtime.ReadMemStats(&stat)
	fmt.Printf("last::%d\n", stat.HeapInuse)
}

func TestGetMessage(t *testing.T) {
	msg := "test message"
	e := Debug().WithMessage(msg).WithMessage(msg)
	
	msgNew := e.GetMessage()
	
	if msg != msgNew {
		t.Errorf("Ошибка #1: msg=%s, msgNew=%s\n", msg, msgNew)
	}
}

func TestName2(t *testing.T) {
	var stat runtime.MemStats
	for j := 0; j < 10; j++ {
		for i := 0; i < 1_000; i++ {
			e := &Message{}
			e.level = 7
			e = nil
		}
		runtime.ReadMemStats(&stat)
		fmt.Printf("%d::%d\n", j, stat.HeapInuse)
	}
	runtime.GC()
	runtime.ReadMemStats(&stat)
	fmt.Printf("last::%d\n", stat.HeapInuse)
}

func BenchmarkLL1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := Debug().WithMessage("qwe")
		e.MaskWords([]string{"qwe"})
		e = nil
	}
}
func BenchmarkLL2(b *testing.B) {
	var e *Message
	for i := 0; i < b.N; i++ {
		e = Debug().WithMessage("qwe")
		e.MaskWords([]string{"qwe"})
	}
}

func BenchmarkLL3(b *testing.B) {
	var stat runtime.MemStats
	for i := 0; i < b.N; i++ {
		e := Debug().WithMessage("qwe")
		e.level = 3
		e.maskedWords = nil
		e = nil
		if i%10_000 == 0 {
			runtime.ReadMemStats(&stat)
			fmt.Printf("%010d::%d\n", i, stat.HeapInuse)
		}
	}
	// runtime.GC()
	// time.Sleep(3 * time.Second)
	runtime.ReadMemStats(&stat)
	fmt.Printf("%010d::%d\n", 999, stat.HeapInuse)
}
func BenchmarkLL4(b *testing.B) {
	var stat runtime.MemStats
	for i := 0; i < b.N; i++ {
		e := Debug().WithMessage("qwe")
		e.level = 3
		// e.maskedWords = nil
		// e = nil
		if i%1000 == 0 {
			runtime.ReadMemStats(&stat)
			fmt.Printf("%05d::%d\n", i, stat.HeapInuse)
		}
	}
	// runtime.GC()
	// time.Sleep(3 * time.Second)
	// runtime.ReadMemStats(&stat)
	// fmt.Printf("%05d::%d\n", 999, stat.HeapInuse)
}

func BenchmarkBytesBufWithReset(b *testing.B) {
	var stat runtime.MemStats
	for i := 0; i < 100; i++ {
		bb := bytes.NewBuffer([]byte("qwerty"))
		bb.WriteString("ASD")
		bb.Reset()
		if i%10 == 0 {
			runtime.ReadMemStats(&stat)
			fmt.Printf("%05d::%d\n", i, stat.HeapInuse)
		}
	}
}
func BenchmarkBytesBufWOReset(b *testing.B) {
	var stat runtime.MemStats
	for i := 0; i < 100; i++ {
		bb := bytes.NewBuffer([]byte("qwerty"))
		bb.WriteString("ASD")
		// bb.Reset()
		if i%10 == 0 {
			runtime.ReadMemStats(&stat)
			fmt.Printf("%05d::%d\n", i, stat.HeapInuse)
		}
	}
}

func BenchmarkStringVSBuilder1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		a := "qwe"
		a += "asd"
	}
}
func BenchmarkStringVSBuilder2(b *testing.B) {
	var a strings.Builder
	for i := 0; i < b.N; i++ {
		a.WriteString("qwe")
		a.WriteString("asd")
	}
}
func BenchmarkStringVSBuilder3(b *testing.B) {
	var a strings.Builder
	for i := 0; i < b.N; i++ {
		a.WriteString("qwe")
		a.WriteString("asd")
		a.Reset()
	}
}

func TestMessage_GetError(t *testing.T) {
	type fields struct {
		pc int
		e  error
	}
	e := errors.New("not nil error")
	tests := []struct {
		name    string
		fields  fields
		wantErr error
	}{
		{name: "nil", fields: fields{pc: 1, e: nil}, wantErr: nil},
		{name: "not nil", fields: fields{pc: 2, e: e}, wantErr: e},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := (&Message{privateCode: tt.fields.pc}).WithError(tt.fields.e).GetError()
			if (tt.wantErr != nil) && !errors.Is(got, tt.wantErr) {
				t.Errorf("GetError() error = %v, wantErr %v", got, tt.wantErr)
			}
		})
	}
}
