package eerrors

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

// LogPrefixType - тип для logPrefix
type LogPrefixType string

const (
	_zero = iota
	_one
	_two
	ERROR
	WARNING
	NOTICE
	INFO
	DEBUG
)

const logPrefixSize = 8

const maskedString = "***"

var (
	moduleName string
	list       map[int]*Message
	//                 0  1  2       3       4         5     6      7
	levels                 = []string{"", "", "", "ERROR", "WARNING", "NOTICE", "INFO", "DEBUG"}
	clearARE, clearSpaceRE *regexp.Regexp
	lock                   sync.RWMutex
	currentLogLevel        int
)

// GetCurLogLevel - возвращаем текущий LogLevel
func GetCurLogLevel() int {
	lock.RLock()
	defer lock.RUnlock()
	return currentLogLevel
}

// SetCurLogLevel - установка уровня логирования
func SetCurLogLevel(lev int) {
	lock.Lock()
	defer lock.Unlock()
	currentLogLevel = lev
}

func (lp LogPrefixType) String() string {
	return string(lp)
}

type ShowMessage struct {
	PublicCode    int    `json:"errorCode"`
	PublicMessage string `json:"errorMessage"`
}

type Message struct {
	privateCode    int
	publicCode     int
	privateMessage string
	publicMessage  string
	t              *time.Time
	woTimeFlag     bool
	level          int
	fName          *string
	fnName         *string
	strNum         *int
	e              error
	lp             *LogPrefixType
	message        *string
	hideModuleName bool
	result         *string
	maskedWords    []string
}

func init() {
	list = make(map[int]*Message)
	clearARE = regexp.MustCompile(`[\n]`)
	clearSpaceRE = regexp.MustCompile(`\s{1,}`)
	rand.Seed(time.Now().UnixNano())
	currentLogLevel = 7
}

func (msg Message) prepareOld() string {
	var (
		tmpl = ""
		attr []interface{}
	)
	if msg.lp != nil {
		tmpl = tmpl + "%s"
		attr = append(attr, *msg.lp)
	}
	
	if msg.privateCode != 0 {
		tmpl = tmpl + " %d"
		attr = append(attr, msg.privateCode)
	}
	
	tmpl = tmpl + " %s"
	attr = append(attr, msg.privateMessage)
	
	if msg.e != nil {
		tmpl = tmpl + " %s"
		attr = append(attr, msg.e.Error())
	}
	
	if msg.message != nil {
		tmpl = tmpl + " %s"
		attr = append(attr, *msg.message)
	}
	
	if msg.fnName != nil && msg.fName != nil {
		if msg.strNum == nil {
			tmpl = tmpl + " @[%s:%s]"
			attr = append(attr, *msg.fName, *msg.fnName)
		} else {
			tmpl = tmpl + " @[%s:%s:%d]"
			attr = append(attr, *msg.fName, *msg.fnName, *msg.strNum)
		}
	}
	return fmt.Sprintf(tmpl, attr...)
}
func (msg *Message) prepare() string {
	if msg.result != nil && len(*msg.result) > 0 {
		return *msg.result
	}
	lock.RLock()
	if msg.level > currentLogLevel {
		lock.RUnlock()
		return ""
	}
	lock.RUnlock()
	var (
		ret strings.Builder
	)
	defer ret.Reset()
	if msg.lp != nil {
		ret.WriteString(string(*msg.lp))
	}
	
	ret.WriteString(" " + levels[msg.level])
	
	if msg.privateCode != 0 {
		ret.WriteString(" " + strconv.Itoa(msg.privateCode))
	}
	
	ret.WriteString(" " + msg.privateMessage)
	
	if msg.e != nil {
		ret.WriteString(" " + msg.e.Error())
	}
	
	if msg.message != nil {
		ret.WriteString(" " + *msg.message)
	}
	
	if msg.fnName != nil && msg.fName != nil {
		if msg.strNum == nil {
			ret.WriteString(" @[" + *msg.fName + ":" + *msg.fnName + "]")
		} else {
			ret.WriteString(" @[" + *msg.fName + ":" + *msg.fnName + ":" + strconv.Itoa(*msg.strNum) + "]")
		}
	}
	s := ret.String()
	
	for _, word := range msg.maskedWords {
		s = strings.Replace(s, word, maskedString, -1)
	}
	
	msg.result = &s
	return s
}

// GetLogLevel - возвращает название уровня логирования
func GetLogLevel(l int) string {
	return levels[l]
}

func getTimeString(t time.Time) string {
	return t.Format("2006-01-02T15:04:05.000000Z07:00")
}

// SetModuleName - установка или снятие вывода названия модуля
func SetModuleName(mName string) {
	if len(mName) > 0 {
		moduleName = mName + " "
	} else {
		moduleName = ""
	}
}

func (msg *Message) String() string {
	return msg.Error()
}

// Message - вывод ошибки для лога
func (msg *Message) Error() string {
	if msg.t == nil {
		return msg.prepare()
	}
	mName := ""
	if len(moduleName) > 0 && !msg.hideModuleName {
		mName = moduleName
	}
	return "[" + getTimeString(*msg.t) + "] " + mName + msg.prepare()
}

func PublicError(err error) (int, string) {
	switch in := err.(type) {
	case *Message:
		return in.PublicError()
	default:
		return 500, err.Error()
	}
}

// PublicError - Вывод публичной текстовой ошибки
func (msg Message) PublicError(in ...interface{}) (int, string) {
	s := msg.publicMessage
	if len(in) > 0 {
		s = fmt.Sprintf(msg.publicMessage, in...)
	}
	
	if len(s) == 0 && msg.message != nil && len(*msg.message) > 0 {
		s = *msg.message
		s = strings.Trim(s, " []")
	}
	
	for _, word := range msg.maskedWords {
		s = strings.Replace(s, word, maskedString, -1)
	}
	
	if msg.privateCode > 0 {
		s += " (" + strconv.Itoa(msg.privateCode) + ")"
	}
	
	return msg.publicCode, s
}

func PrivateError(err error) (int, string) {
	switch in := err.(type) {
	case *Message:
		return in.PrivateError()
	default:
		return 500, err.Error()
	}
}

// PrivateError - Вывод приватной текстовой ошибки
func (msg Message) PrivateError(in ...interface{}) (int, string) {
	s := msg.privateMessage
	if len(in) > 0 {
		s = fmt.Sprintf(msg.privateMessage, in...)
	}
	
	if len(s) == 0 && msg.message != nil && len(*msg.message) > 0 {
		s = *msg.message
		s = strings.Trim(s, " []")
	}
	
	for _, word := range msg.maskedWords {
		s = strings.Replace(s, word, maskedString, -1)
	}
	
	if msg.privateCode == 0 {
		return 500, s
	}
	
	return msg.privateCode, s
}

// PrivateErrorWithErr - Вывод приватной текстовой ошибки + error
func (msg Message) PrivateErrorWithErr(in ...interface{}) (int, string) {
	s := msg.privateMessage
	if len(in) > 0 {
		s = fmt.Sprintf(msg.privateMessage, in...)
	}
	
	if len(s) == 0 && msg.message != nil && len(*msg.message) > 0 {
		s = *msg.message
		s = strings.Trim(s, " []")
	}
	
	for _, word := range msg.maskedWords {
		s = strings.Replace(s, word, maskedString, -1)
	}
	
	if msg.e != nil {
		s = s + "::" + msg.e.Error()
	}
	
	if msg.privateCode == 0 {
		return 500, s
	}
	
	return msg.privateCode, s
}

// JSON - публичная ошибка в JSON
func (msg Message) JSON() []byte {
	sMsg := ShowMessage{PublicMessage: msg.publicMessage, PublicCode: msg.publicCode}
	j, _ := json.Marshal(sMsg)
	return j
}

// TODO: publicMessage д. иметь плейсхолдеры
// New - новый шаблон сообщения
func New(privateCode, publicCode int, privateMessage, publicMessage string, level int) (*Message, bool) {
	if privateCode == 0 {
		return nil, false
	}
	e := &Message{
		privateCode:    privateCode,
		publicCode:     publicCode,
		privateMessage: privateMessage,
		publicMessage:  publicMessage,
		level:          level,
	}
	if exist, ok := list[privateCode]; ok {
		return exist, false
	}
	list[privateCode] = e
	
	return e, true
}

func (msg Message) IsEq(err Message) bool {
	return msg.privateCode == err.privateCode
}

func (msg Message) Is(e interface{}) bool {
	var (
		eerr *Message
	)
	
	switch e.(type) {
	case int:
		eerr = Get(e.(int))
	case int64:
		eerr = Get(int(e.(int64)))
	case Message:
		errValue := e.(Message)
		eerr = &errValue
	case *Message:
		eerr = e.(*Message)
	default:
		return false
	}
	
	if eerr == nil {
		return false
	}
	
	return msg.IsEq(*eerr)
}

// Add - добавить готовую ошибку в словарь
func (msg Message) Add() {
	if msg.privateCode == 0 {
		return
	}
	list[msg.privateCode] = &msg
}

// WOModuleName - убираем moduleName из вывода
func (msg *Message) WOModuleName() *Message {
	msg.hideModuleName = true
	return msg
}

// WOCode - убираем privateCode
func (msg *Message) WOCode() *Message {
	msg.privateCode = 0
	return msg
}

// WOTime - убираем время из вывода (нужно для вывода в syslog, например)
func (msg *Message) WOTime() *Message {
	msg.t = nil
	msg.woTimeFlag = true
	return msg
}

// WithMessage - устанавливаем необятельный параметр mes
func (msg *Message) WithMessage(mes ...interface{}) *Message {
	msg.checkT()
	msg.result = nil
	outStr := fmt.Sprint(" ", mes)
	// outStr = string(clearSpaceRE.ReplaceAll(clearARE.ReplaceAll([]byte(outStr), []byte("")), []byte(" ")))
	msg.message = &outStr
	return msg
}

// Sprintf - заполнение плейсхолдеров в privateMessage и publicMessage
//  надо помнить, что можно использовать конструкции типа "%[1]s"
func (msg *Message) Sprintf(in ...interface{}) *Message {
	outStr := msg.privateMessage
	if len(in) > 0 {
		outStr = fmt.Sprintf(msg.privateMessage, in...)
	}
	msg.privateMessage = outStr
	
	outStr = msg.publicMessage
	if len(in) > 0 {
		outStr = fmt.Sprintf(msg.publicMessage, in...)
	}
	msg.publicMessage = outStr
	
	return msg
}

// SprintfPriv - заполнение плейсхолдеров в только в privateMessage
//  надо помнить, что можно использовать конструкции типа "%[1]s"
func (msg *Message) SprintfPriv(in ...interface{}) *Message {
	outStr := msg.privateMessage
	if len(in) > 0 {
		outStr = fmt.Sprintf(msg.privateMessage, in...)
	}
	
	msg.privateMessage = outStr
	
	return msg
}

// WithPrefix - устанавливаем необятельный параметр logPrefix
func (msg *Message) WithPrefix(lp LogPrefixType) *Message {
	msg.checkT()
	msg.result = nil
	msg.lp = &lp
	return msg
}

// WithStringNum - устанавливаем необятельный параметр номер строки с ошибкой
func (msg *Message) WithStringNum(strNum int) *Message {
	msg.checkT()
	msg.result = nil
	msg.strNum = &strNum
	return msg
}

// WithError - устанавливаем необятельный параметр error
func (msg *Message) WithError(err error) *Message {
	if err == nil {
		return msg
	}
	msg.checkT()
	msg.result = nil
	msg.e = err
	return msg
}

// Level - возвращаем уровнь ошибки
func (msg *Message) Level() int {
	return msg.level
}

// SetLevel - устанавливаем уровень сообщения
//  основное предназначение: собираем сообщения с уровем логирования Debug, если произошла ошибка, то можно у собранных сообщений сменить уровень на 3  и вывести, если все нормально, то выводим с уровнем 7
func (msg *Message) SetLevel(newLevel int) *Message {
	msg.checkT()
	msg.level = newLevel
	return msg
}

// WithAt - устанавливаем необятельный параметр fName fnName
func (msg *Message) WithAt(fName, fnName string) *Message {
	msg.checkT()
	msg.result = nil
	msg.fName = &fName
	msg.fnName = &fnName
	return msg
}

// MaskWords - заменяем слова на * чтобы маскировать эти слова при записи в лог
func (msg *Message) MaskWords(words []string) *Message {
	msg.checkT()
	msg.result = nil
	msg.maskedWords = append(msg.maskedWords, words...)
	
	return msg
}

// MustGet - вернет сообщение и true если сообщение найдено в словаре ошибок
func MustGet(code int) (*Message, bool) {
	e, ok := list[code]
	return e, ok
}

func (msg *Message) Reset() {
	msg.fName = nil
	msg.fnName = nil
	msg.strNum = nil
	msg.e = nil
	msg.lp = nil
	msg.message = nil
	msg.result = nil
	msg.maskedWords = nil
}

// Get - получаем сообщение, являющееся точной копией исходного
func Get(code int) *Message {
	if code <= 0 {
		if code == 0 {
			return getTextMessage(DEBUG)
		}
		return getTextMessage(code * -1)
	}
	e, ok := MustGet(code)
	if !ok {
		return &Message{}
	}
	tNow := time.Now()
	return &Message{
		privateCode:    e.privateCode,
		publicCode:     e.publicCode,
		privateMessage: e.privateMessage,
		publicMessage:  e.publicMessage,
		t:              &tNow,
		level:          e.level,
	}
}

// Getf - получаем сообщение, являющееся точной копией исходного
//  с заполнением плейсхолдеров в privateMessage и publicMessage
//  надо помнить, что можно использовать конструкции типа "%[1]s"
func Getf(code int, in ...interface{}) *Message {
	if code <= 0 {
		if code == 0 {
			return getTextMessage(DEBUG)
		}
		return getTextMessage(code * -1)
	}
	e, ok := MustGet(code)
	if !ok {
		return &Message{}
	}
	tNow := time.Now()
	
	outStrPriv := e.privateMessage
	if len(in) > 0 {
		outStrPriv = fmt.Sprintf(e.privateMessage, in...)
	}
	
	outStrPub := e.publicMessage
	if len(in) > 0 {
		outStrPub = fmt.Sprintf(e.publicMessage, in...)
	}
	
	return &Message{
		privateCode:    e.privateCode,
		publicCode:     e.publicCode,
		privateMessage: outStrPriv,
		publicMessage:  outStrPub,
		t:              &tNow,
		level:          e.level,
	}
}

// GetPrivf - получаем сообщение, являющееся точной копией исходного
//  с заполнением плейсхолдеров в privateMessage
//  надо помнить, что можно использовать конструкции типа "%[1]s"
func GetPrivf(code int, in ...interface{}) *Message {
	if code <= 0 {
		if code == 0 {
			return getTextMessage(DEBUG)
		}
		return getTextMessage(code * -1)
	}
	e, ok := MustGet(code)
	if !ok {
		return &Message{}
	}
	tNow := time.Now()
	
	outStrPriv := e.privateMessage
	if len(in) > 0 {
		outStrPriv = fmt.Sprintf(e.privateMessage, in...)
	}
	
	return &Message{
		privateCode:    e.privateCode,
		publicCode:     e.publicCode,
		privateMessage: outStrPriv,
		publicMessage:  e.publicMessage,
		t:              &tNow,
		level:          e.level,
	}
}

func getTextMessage(l int) *Message {
	t := time.Now()
	return &Message{
		privateCode:    0,
		publicCode:     0,
		privateMessage: "",
		publicMessage:  "",
		t:              &t,
		level:          l,
	}
}

// Decorate - подготовить ошибку для вывода
//      code int - код шаблонной ошибки (privateCode)
//      fName - имя файла, где произошла ошибка
//      fnName string - имя функции, где произошла ошибка
func Decorate(code int, fName, fnName string) *Message {
	e, ok := list[code]
	if !ok {
		return &Message{}
	}
	t := time.Now()
	return &Message{
		privateCode:    e.privateCode,
		publicCode:     e.publicCode,
		privateMessage: e.privateMessage,
		fName:          &fName,
		fnName:         &fnName,
		publicMessage:  e.publicMessage,
		t:              &t,
		level:          e.level,
	}
}

func time2rel(t time.Time) *time.Time {
	return &t
}

// Debug - получение сообщнения уровня Debug
func Debug() *Message {
	m := Get(-7)
	m.t = time2rel(time.Now())
	m.result = nil
	return m
}

// Error - получение сообщнения уровня Error
func Error() *Message {
	m := Get(-3)
	m.t = time2rel(time.Now())
	m.result = nil
	return m
}

// Info - получение сообщнения уровня Info
func Info() *Message {
	m := Get(-6)
	m.t = time2rel(time.Now())
	m.result = nil
	return m
}

// Warning - получение сообщнения уровня Warning
func Warning() *Message {
	m := Get(-4)
	m.t = time2rel(time.Now())
	m.result = nil
	return m
}

// Notice - получение сообщнения уровня Notice
func Notice() *Message {
	m := Get(-5)
	m.t = time2rel(time.Now())
	m.result = nil
	return m
}

func (msg *Message) checkT() {
	if !msg.woTimeFlag {
		if msg.t == nil || msg.privateCode > 0 {
			msg.t = time2rel(time.Now())
		}
	}
}

func randString(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// NewPrefix - получаем новый LogPrefix
func NewPrefix() LogPrefixType {
	return LogPrefixType(randString(logPrefixSize))
}

// AddPrefix - расширяем LogPrefix. В конеце обязательно будет добавлено новый кусок
//      можно передать еще несколько строк, которые будут добавлены в выходной logPrefix
func AddPrefix(origin LogPrefixType, in ...string) LogPrefixType {
	ret := strings.Builder{}
	defer ret.Reset()
	ret.WriteString(string(origin))
	if len(in) > 0 {
		for _, i := range in {
			ret.WriteString("::" + string(i))
		}
	}
	ret.WriteString("::" + randString(logPrefixSize))
	
	return LogPrefixType(ret.String())
}

// NewStringPrefix - получаем новый LogPrefix из строки
func NewStringPrefix(in string) LogPrefixType {
	return LogPrefixType(in)
}

// AddStringPrefix - расширяем LogPrefix переданной строкой
func AddStringPrefix(origin LogPrefixType, in string) LogPrefixType {
	return LogPrefixType(string(origin) + "::" + in)
}

// GetText - получаем только текст сообщения
func (msg *Message) GetText() string {
	var ret []string
	
	if msg.message != nil {
		ret = append(ret, *msg.message)
	}
	
	if msg.e != nil {
		ret = append(ret, msg.e.Error())
	}
	
	return strings.Join(ret, "::")
}

// GetMessage - получаем только текст, добавленный через WithMessage
func (msg *Message) GetMessage() string {
	
	if msg.message == nil {
		return ""
	}
	
	return strings.TrimSuffix(strings.TrimPrefix(*msg.message, " ["), "]")
	
}

// GetError возвращает ошибку, добавленную через &Message.WithError
// или nil, если &Message.WithError не был вызван или в него был передан nil явно
func (msg *Message) GetError() error {
	return msg.e
}
